// KEY
const key = ''; // AJOUTEZ CLE ICI !!!

// HTML SELECTEURS
const submit = document.getElementById('btn_submit');
const input_enter = document.getElementById('search_input');

// ID FILM RECHERCHE & GENRE
const title_url = localStorage.getItem('title');
const genre = localStorage.getItem('genre');

// PAGE
let page = 1;
const total_pages = localStorage.getItem('total_pages');
const result = document.getElementById('search');
const page_index = document.getElementById('page_index');

// FONCTION SEARCH
const search = () => {
  // Sélectionner l'élément input et récupérer sa valeur
  let input = '';
  input = document.getElementById('search_input').value;
  localStorage.setItem('search', input);
  // Ajoute des "+" entre chaque mot pour la recherche dans l'API
  input = input.split(' ').join('+');
  localStorage.setItem('title', input);
};

// FONCTION SEARCH SUR TOUCHE "ENTREE"
input_enter.addEventListener('keydown', (cmd) => {
  // SI L'ENTREE CONTIENT AU MOINS UN CARACTERE
  if (input_enter.value.length) {
    search();

    // BOUTON ENTRER
    if (cmd.keyCode === 13) {
      search();
    }
  }
});

// CHERCHE AU CLIC
submit.addEventListener('click', () => {
  search();
});

//////////// FONCTION AFFICHAGE //////////////
const display = () => {
  // RECUPERE LE TOTAL DE PAGES
  fetch(`https://api.themoviedb.org/3/search/${genre}?api_key=${key}&query=${title_url}&language=fr&include_adult=false`)
    .then((response) => {
      if (response) {
        return response.json();
      }
    })
    .then((transformation) => {
      // STEP 2 : RENVOIE LES FILMS
      localStorage.setItem('total_pages', transformation.total_pages);
    });
  // RECUPERE LES DONNEES
  fetch(`https://api.themoviedb.org/3/search/${genre}?api_key=${key}&query=${title_url}&page=${page}&language=fr&include_adult=false`)
    .then((response) => {
      if (response) {
        return response.json();
      }
    })
    .then((transformation) => {
      // RESULTATS RECHERCHE
      const movie = transformation.results;
      result.innerText = `Résultats pour : "${localStorage.getItem('search')}" / Page ${page}-${localStorage.getItem('total_pages')}`;

      // CONTENU CREATION DIV
      for (let i = 0; i < transformation.results.length; ++i) {
        ////////////// RECUPERE LES INFORMATIONS DU FILM /////////////////

        // RECUPERE LE TITRE
        let title = movie[i].title !== undefined ? movie[i].title
          : movie[i].original_title;
        title = title !== undefined ? title : movie[i].name;

        // RECUPERE L'IMAGE
        const poster_path = movie[i].poster_path !== null
          ? `https://image.tmdb.org/t/p/w500/${movie[i].poster_path}` : 'prod/assets/images/confused.png';

        // RECUPERE L'ID
        const id = movie[i].id;

        // RECUPERE LA DATE DE SORTIE
        let date = transformation.results[i].release_date !== undefined
          ? transformation.results[i].release_date : transformation.results[i].first_air_date;
        const seo_date = date;
        date = new Date(date).toLocaleDateString();

        // RECUPERE LA MOYENNE, FORMAT (ex.): 0.75 & "75%";
        const average = `${transformation.results[i].vote_average * 10}%`;

        let card_deck;

        const card_maker = () => {
          if (i < 2) {
            card_deck = document.getElementById('deck_1');
          } else if (i < 5) {
            card_deck = document.getElementById('deck_2');
          } else if (i < 8) {
            card_deck = document.getElementById('deck_3');
          } else if (i < 11) {
            card_deck = document.getElementById('deck_4');
          } else if (i < 14) {
            card_deck = document.getElementById('deck_5');
          } else if (i < 17) {
            card_deck = document.getElementById('deck_6');
          } else {
            card_deck = document.getElementById('deck_7');
          }

          const card = document.createElement('div');

          // HTML ELEMENTS //
          const card_head = document.createElement('div');
          const card_body = document.createElement('div');
          card.setAttribute('itemscope', ''); // SCHEMA
          card.setAttribute('itemtype', 'http://schema.org/Movie'); // SCHEMA
          if (movie.length === 1) {
            card.classList = 'card mb-5 col-md-4';
          } else {
            card.classList = 'card mb-5';
          }
          card_head.classList = 'card-header w-auto';
          card_body.classList = 'card-body';

          // CREE LES DIV
          card.appendChild(card_head);
          card.appendChild(card_body);

          // AFFICHE
          const poster_link = document.createElement('a');
          const poster = document.createElement('img');
          poster_link.setAttribute('href', './movie.html');
          poster.classList = 'card-img-top h-auto';
          poster.setAttribute('itemprop', 'image'); // SCHEMA
          poster.setAttribute('src', poster_path);
          poster.setAttribute('alt', `affiche du film: "${title}"`);
          poster_link.appendChild(poster);
          card_head.appendChild(poster_link);

          // TITRE
          const head = document.createElement('h4');
          head.setAttribute('itemprop', 'name'); // SCHEMA
          head.innerText = title;
          card_body.appendChild(head);

          // DATE DE SORTIE
          const release_date = document.createElement('h5');
          release_date.setAttribute('itemprop', 'dateCreated'); // SCHEMA
          release_date.setAttribute('datetime', `${seo_date}`);
          release_date.innerText = date;
          card_body.appendChild(release_date);

          card_deck.appendChild(card);

          // RECUPERE L'ID AU CLIC
          poster_link.addEventListener('click', () => {
            sessionStorage.setItem('movie_id', id);
            sessionStorage.setItem('poster_path', poster_path);
            sessionStorage.setItem('title', title);
            sessionStorage.setItem('date', date);
            sessionStorage.setItem('score', average);
          });
        };

        // APPELLE LE CREATEUR DE CARDS A CHAQUE ITERATION
        card_maker();
      }
    });
};

// BOUTONS PAGINATION
const next = document.getElementById('next');
const prev = document.getElementById('prev');

///////////////// CHANGE DE PAGE ///////////////////
const movies = [document.getElementById('deck_1'),
  document.getElementById('deck_2'), document.getElementById('deck_3'),
  document.getElementById('deck_4'), document.getElementById('deck_5'),
  document.getElementById('deck_6'), document.getElementById('deck_7'),
];

// NEXT
// eslint-disable-next-line no-loop-func
next.addEventListener('click', (e) => {
  console.log(e);
  if (page < total_pages) {
    ++page;
    page_index.innerText = `${page}`;
    // SUPPRIME LES ELEMENTS EXISTANTS
    for (const movie of movies) {
      movie.innerHTML = '';
    }
    display();
  }
});
// PREV
// eslint-disable-next-line no-loop-func
prev.addEventListener('click', () => {
  if (page >= 2) {
    --page;
    page_index.innerText = `${page}`;
    // SUPPRIME LES ELEMENTS EXISTANTS
    for (const movie of movies) {
      movie.innerHTML = '';
    }
    display();
  }
});

// LANCEMENT PAGE RECHERCHES
window.onload = display();
