// KEY
const key = ''; // AJOUTEZ CLE ICI !!!

// RECUPERER LES ELEMENTS DANS LA S.STORAGE APRES LE CLIC
const movie_id = sessionStorage.getItem('movie_id');
const title = sessionStorage.getItem('title');
const date = sessionStorage.getItem('date');
const score = sessionStorage.getItem('score');
const genre = localStorage.getItem('genre');

// CIRCLE ANIM

// RENVOIE LES PROPRIETES GENERALES DU FILM
const movie_general = () => {
  fetch(`https://api.themoviedb.org/3/${genre}/${movie_id}?api_key=${key}&language=fr`)

    .then((response) => {
      if (response) {
        return response.json();
      }
    })

    .then((movie) => {
      let poster_path = sessionStorage.getItem('poster_path');
      if (sessionStorage.getItem('poster_path') === 'prod/assets/images/confused.png') {
        poster_path = null;
      }
      // VARIABLES GENERALES FILM
      const movie_bg = `https://image.tmdb.org/t/p/original/${movie.backdrop_path}`; // BACKGROUND
      const year = date.split('/')[2]; // ANNEE DE SORTIE
      const lang = movie.original_language.toUpperCase(); // LANGUE ORIGINALE
      const genres_list = movie.genres; // GENRES
      let genres = [];
      const tag = movie.tagline; // PUNCHLINE
      const overview = movie.overview; // SYNOPSIS

      // RUNTIME
      let runtime = movie.runtime;
      if (runtime === 0) {
        runtime = 'Durée indéfinie';
      } else {
        const minutes = runtime % 60;
        const hours = (runtime - minutes) / 60;

        // CHANGE RUNTIME => ..h..min;
        if (hours === 0) { // MOINS D'UNE HEURE
          runtime = `${minutes}min`;
        } else if (hours !== 0 && minutes === 0) { // PLUS D'UNE HEURE ET ZERO MINUTES
          runtime = `${hours}h00`;
        } else if (hours !== 0 && minutes < 10) { // PLUS D'UNE HEURE ET MOINS DE DIX MINUTES
          runtime = `${hours}h0${minutes}min`;
        } else if (hours !== 0 && minutes >= 10) { // PLUS D'UNE HEURE ET PLUS DE DIX MINUTES
          runtime = `${hours}h${minutes}min`;
        } else {
          runtime = 'Durée indéfinie';
        }
      }

      // LISTE LES GENRES
      for (let i = 0; i < genres_list.length; ++i) {
        genres.push(genres_list[i].name);
      }

      // CREE UNE STRING DES GENRES
      genres = genres.join(', ');

      // HTML
      // AJOUTE LE BACKGROUND AU HTML
      const bg_div = document.getElementById('backdrop_path');
      bg_div.style.backgroundImage = `url(${movie_bg})`;
      // AJOUTE L'EN TËTE
      const title_div = document.getElementById('title');
      title_div.innerHTML = `${title}<span id="year"> (${year})</span>`;
      // AJOUTE LES SPECS PRINCIPALES
      const specs_div = document.getElementById('main_specs');
      specs_div.innerText = `${date}(${lang}) - ${genres} - ${runtime}`;
      // AJOUTE LA PUNCHLINE
      const punchline_div = document.getElementById('punchline');
      if (tag) {
        punchline_div.innerHTML = `<span class="quote">"</span> ${tag} <span class="quote">"</span>`;
      }
      // AJOUTE LA POPULARITE
      const average = document.getElementById('average');
      average.setAttribute('itemprop', 'aggregateRating');
      average.innerText = `${score}`;
      // AJOUTE LE SYNOPSIS
      const overview_div = document.getElementById('overview');
      if (overview !== '') {
        overview_div.innerText = overview;
      } else {
        overview_div.innerText = 'Ce film ne propose pas de synopsis à l\'heure actuelle, désolé !';
      }
      // AJOUTE L'IMAGE
      const img_div = document.getElementById('poster');
      img_div.setAttribute('src', `${poster_path}`);
    });
};

// RENVOIE LE NOM DU REALISATEUR
const movie_credits = () => {
  fetch(`https://api.themoviedb.org/3/${genre}/${movie_id}/credits?api_key=${key}`)

    .then((response) => {
      // STEP 1
      if (response) {
        return response.json();
      }
    })
    .then((movie) => {
      // CASTING PRINCIPAL (3 premiers acteurs)
      const movie_cast = movie.cast;
      let movie_cast_list = [];
      movie_cast.length = 3;
      for (let i = 0; i < 3; ++i) {
        movie_cast_list.push(movie_cast[i].name);
      }
      movie_cast_list = movie_cast_list.join(', ');

      // REALISATEUR
      let director = '';
      for (let i = 0; i < movie.crew.length; ++i) {
        if (movie.crew[i].job === 'Director') {
          director = movie.crew[i].name;
        }
      }

      // HTML
      // AJOUTE LE REALISATEUR
      const director_div = document.getElementById('director');
      director_div.innerText = director;
      // AJOUTE LE CASTING
      const cast_div = document.getElementById('cast');
      cast_div.innerText = movie_cast_list;
    });
};

const movie_recommandations = () => {
  fetch(`https://api.themoviedb.org/3/${genre}/${movie_id}/recommendations?api_key=${key}&language=fr&page=1`)
    .then((result) => {
      if (result) {
        return result.json();
      }
    })
    .then((transform) => {
      const recommendations = transform.results;
      recommendations.length = 3;
      console.log(recommendations);

      for (let i = 0; i < 6; ++i) {
        // RECUPERE L'AFFICHE
        const poster_path_r = `https://image.tmdb.org/t/p/original/${recommendations[i].poster_path}`;

        // RECUPERE LE TITRE
        let title_r = recommendations[i].title !== undefined ? recommendations[i].title
          : recommendations[i].original_title;
        title_r = title_r !== undefined ? title_r : recommendations[i].name;

        // RECUPERE L'ID
        const id_r = recommendations[i].id;

        // RECUPERE LA DATE DE SORTIE
        let date_r = recommendations[i].release_date !== undefined
          ? recommendations[i].release_date : recommendations[i].first_air_date;
        const seo_date_r = date_r;
        date_r = new Date(date_r).toLocaleDateString();

        // RECUPERE LA MOYENNE, FORMAT : 0.00 & "00%"
        const average_r = `${recommendations[i].vote_average * 10}%`;

        ////////////////////////// HTML ///////////////////////////////
        // DECK 1
        const deck = document.getElementById('recommendations_deck');

        // HTML ELEMENTS
        const card = document.createElement('div');
        const card_head = document.createElement('div');
        const card_body = document.createElement('div');
        card.setAttribute('itemscope', ''); // SCHEMA
        card.setAttribute('itemtype', 'http://schema.org/Movie'); // SCHEMA
        card.classList = 'card mb-5';
        card_head.classList = 'card-header w-auto';
        card_body.classList = 'card-body';

        // CREE LES DIV
        deck.appendChild(card);
        card.appendChild(card_head);
        card.appendChild(card_body);

        // AFFICHE
        const poster_link = document.createElement('a');
        const poster = document.createElement('img');
        poster_link.setAttribute('href', './movie.html');
        poster.classList = 'card-img-top h-auto';
        poster.setAttribute('itemprop', 'image'); // SCHEMA
        poster.setAttribute('src', poster_path_r);
        poster.setAttribute('alt', `affiche du film: "${title}"`);
        poster_link.appendChild(poster);
        card_head.appendChild(poster_link);

        // TITRE
        const head = document.createElement('h4');
        head.setAttribute('itemprop', 'name'); // SCHEMA
        head.innerText = title_r;
        card_body.appendChild(head);

        // DATE DE SORTIE
        const release_date = document.createElement('h5');
        release_date.setAttribute('itemprop', 'dateCreated'); // SCHEMA
        release_date.setAttribute('datetime', `${seo_date_r}`);
        release_date.innerText = date_r;
        card_body.appendChild(release_date);

        // RECUPERE L'ID AU CLIC
        poster_link.addEventListener('click', () => {
          sessionStorage.setItem('movie_id', id_r);
          sessionStorage.setItem('poster_path', poster_path_r);
          sessionStorage.setItem('title', title_r);
          sessionStorage.setItem('date', date_r);
          sessionStorage.setItem('score', average_r);
        });
      }
    });
};

window.onload = movie_general();
window.onload = movie_credits();
window.onload = movie_recommandations();
