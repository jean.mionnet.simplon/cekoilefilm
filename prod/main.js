// KEY
const key = ''; // AJOUTEZ CLE ICI !!!

// HTML SELECTEURS
const submit = document.getElementById('btn_submit');
const input_enter = document.getElementById('search_input');
const decks = [document.getElementById('deck_1'), document.getElementById('deck_2'),
  document.getElementById('deck_3'), document.getElementById('deck_4')];

// GENRE
let genre = 'movie';
localStorage.setItem('genre', genre);

// GENRE SELECT
const movies_btn = document.getElementById('movies');
const series_btn = document.getElementById('series');

// FONCTION SEARCH
const search = () => {
  // Sélectionner l'élément input et récupérer sa valeur
  let input = '';
  input = document.getElementById('search_input').value;
  localStorage.setItem('search', input);
  // Ajoute des "+" entre chaque mot pour la recherche dans l'API
  input = input.split(' ').join('+');
  localStorage.setItem('title', input);
  localStorage.setItem('genre', genre);
};

// FONCTION SEARCH SUR TOUCHE "ENTREE"
input_enter.addEventListener('keydown', (cmd) => {
  // SI L'ENTREE CONTIENT AU MOINS UN CARACTERE
  if (input_enter.value.length) {
    search();

    // BOUTON ENTRER
    if (cmd.keyCode === 13) {
      search();
    }
  }
});

// CHERCHE AU CLIC
submit.addEventListener('click', () => {
  search();
});

// CIRCLE ANIM
const circle_anim = (id, value) => {
  const bar = new ProgressBar.Circle(`#${id}`, {
    color: '#aaa',
    // PREVIENT LE CLIPPING
    strokeWidth: 4,
    trailWidth: 1,
    easing: 'easeInOut',
    duration: 3000,
    text: {
      autoStyleContainer: false,
    },
    from: { color: '#aaa', width: 1 },
    to: { color: '#333', width: 6 },
    // ETAPES PAR DEFAUT
    step(state, circle) {
      circle.path.setAttribute('stroke', '#8c0327');
      circle.path.setAttribute('stroke-width', state.width);
      circle.path.setAttribute('fill-opacity', '0.8');
      circle.path.setAttribute('fill', '#000');

      // HTML VALUE & TEXT
      const value = Math.round(circle.value() * 100);
      if (value === 0) {
        circle.setText('');
      } else {
        circle.setText(`${value}%`);
      }
    },
  });

  bar.text.style.fontSize = '1.8rem';

  bar.animate(value); // NOTE ENTRE 0 ET 1
};

// FONCTION TENDANCES
const trend = () => {
// FETCH FILMS
  fetch(`https://api.themoviedb.org/3/trending/${genre}/week?api_key=${key}&page=1&include_adult=false`)

    .then((response) => {
    // STEP 1
      if (response) {
        return response.json();
      }

      console.log('Aucune tendances trouvées');
    })

    .then((transformation) => {
    // STEP 2 : RENVOIE LES FILMS
      for (let i = 0; i < 6; ++i) {
        // MAXIMUM 6 TENDANCES
        /////////////////////////// GET //////////////////////////
        const movie = transformation.results;

        // RECUPERE LES 6 TENDANCES DE LA SEMAINE (ALL)
        // RECUPERE L'AFFICHE
        const poster_path = `https://image.tmdb.org/t/p/original/${movie[i].poster_path}`;

        // RECUPERE LE TITRE
        let title = movie[i].title !== undefined ? movie[i].title
          : movie[i].original_title;
        title = title !== undefined ? title : movie[i].name;

        // RECUPERE L'ID
        const id = movie[i].id;

        // RECUPERE LA DATE DE SORTIE
        let date = movie[i].release_date !== undefined
          ? movie[i].release_date : movie[i].first_air_date;
        const seo_date = date;
        date = new Date(date).toLocaleDateString();

        // RECUPERE LA MOYENNE, FORMAT : 0.00 & "00%"
        const average_anim = Math.round(movie[i].vote_average * 10) / 100;
        const average = `${movie[i].vote_average * 10}%`;

        ////////////////////////// HTML ///////////////////////////////
        // DECK 1
        let deck = document.getElementById('deck_1');

        // DECK 2
        if (i >= 3) {
          deck = document.getElementById('deck_2');
        }

        // HTML ELEMENTS
        const card = document.createElement('div');
        const card_head = document.createElement('div');
        const card_body = document.createElement('div');
        card.setAttribute('itemscope', ''); // SCHEMA
        card.setAttribute('itemtype', 'http://schema.org/Movie'); // SCHEMA
        card.classList = 'card mb-5';
        card_head.classList = 'card-header w-auto';
        card_body.classList = 'card-body';

        // CREE LES DIV
        deck.appendChild(card);
        card.appendChild(card_head);
        card.appendChild(card_body);

        // AFFICHE
        const poster_link = document.createElement('a');
        const poster = document.createElement('img');
        poster_link.setAttribute('href', './movie.html');
        poster.classList = 'card-img-top h-auto';
        poster.setAttribute('itemprop', 'image'); // SCHEMA
        poster.setAttribute('src', poster_path);
        poster.setAttribute('alt', `affiche du film: "${title}"`);
        poster_link.appendChild(poster);
        card_head.appendChild(poster_link);

        // MOYENNE CIRCLE
        const vote_circle = document.createElement('div');
        card_head.appendChild(vote_circle);
        vote_circle.classList = 'vote_circle';
        vote_circle.id = `vote_circle_${i}`;
        vote_circle.setAttribute('itemprop', 'aggregateRating'); // SCHEMA
        circle_anim(`vote_circle_${i}`, average_anim);

        // TITRE
        const head = document.createElement('h4');
        head.setAttribute('itemprop', 'name'); // SCHEMA
        head.innerText = title;
        card_body.appendChild(head);

        // DATE DE SORTIE
        const release_date = document.createElement('h5');
        release_date.setAttribute('itemprop', 'dateCreated'); // SCHEMA
        release_date.setAttribute('datetime', `${seo_date}`);
        release_date.innerText = date;
        card_body.appendChild(release_date);

        // RECUPERE L'ID AU CLIC
        poster_link.addEventListener('click', () => {
          sessionStorage.setItem('movie_id', id);
          sessionStorage.setItem('poster_path', poster_path);
          sessionStorage.setItem('title', title);
          sessionStorage.setItem('date', date);
          sessionStorage.setItem('score', average);
        });
      }
    });
};

const top_rated = () => {
  // FETCH FILMS
  fetch(`https://api.themoviedb.org/3/${genre}/top_rated?api_key=${key}&language=fr&page=1&include_adult=false`)
    .then((response) => {
    // STEP 1
      if (response) {
        return response.json();
      }

      console.log('Aucune tendances trouvées');
    })

    .then((transformation) => {
    // STEP 2 : RENVOIE LES FILMS
      for (let i = 0; i < 6; ++i) {
        // MAXIMUM 6 TENDANCES
        /////////////////////////// GET //////////////////////////
        const movie = transformation.results;

        // RECUPERE LES 6 TENDANCES DE LA SEMAINE (ALL)
        // RECUPERE L'AFFICHE
        const poster_path = `https://image.tmdb.org/t/p/original/${movie[i].poster_path}`;

        // RECUPERE LE TITRE
        let title = movie[i].title !== undefined ? movie[i].title
          : movie[i].original_title;
        title = title !== undefined ? title : movie[i].name;

        // RECUPERE L'ID
        const id = movie[i].id;

        // RECUPERE LA DATE DE SORTIE
        let date = movie[i].release_date !== undefined
          ? movie[i].release_date : movie[i].first_air_date;
        const seo_date = date;
        date = new Date(date).toLocaleDateString();

        // RECUPERE LA MOYENNE, FORMAT : 0.00 & "00%"
        const average_anim = Math.round(movie[i].vote_average * 10) / 100;
        const average = `${movie[i].vote_average * 10}%`;

        ////////////////////////// HTML ///////////////////////////////
        // DECK 1
        let deck = document.getElementById('deck_3');

        // DECK 2
        if (i >= 3) {
          deck = document.getElementById('deck_4');
        }

        // HTML ELEMENTS
        const card = document.createElement('div');
        const card_head = document.createElement('div');
        const card_body = document.createElement('div');
        card.setAttribute('itemscope', ''); // SCHEMA
        card.setAttribute('itemtype', 'http://schema.org/Movie'); // SCHEMA
        card.classList = 'card mb-5';
        card_head.classList = 'card-header w-auto';
        card_body.classList = 'card-body';

        // CREE LES DIV
        deck.appendChild(card);
        card.appendChild(card_head);
        card.appendChild(card_body);

        // AFFICHE
        const poster_link = document.createElement('a');
        const poster = document.createElement('img');
        poster_link.setAttribute('href', './movie.html');
        poster.classList = 'card-img-top h-auto';
        poster.setAttribute('itemprop', 'image'); // SCHEMA
        poster.setAttribute('src', poster_path);
        poster.setAttribute('alt', `affiche du film: "${title}"`);
        poster_link.appendChild(poster);
        card_head.appendChild(poster_link);

        // MOYENNE CIRCLE
        const vote_circle = document.createElement('div');
        card_head.appendChild(vote_circle);
        vote_circle.classList = 'vote_circle';
        vote_circle.id = `vote_circle_${i + 6}`;
        vote_circle.setAttribute('itemprop', 'aggregateRating'); // SCHEMA
        circle_anim(`vote_circle_${i + 6}`, average_anim);

        // TITRE
        const head = document.createElement('h4');
        head.setAttribute('itemprop', 'name'); // SCHEMA
        head.innerText = title;
        card_body.appendChild(head);

        // DATE DE SORTIE
        const release_date = document.createElement('h5');
        release_date.setAttribute('itemprop', 'dateCreated'); // SCHEMA
        release_date.setAttribute('datetime', `${seo_date}`);
        release_date.innerText = date;
        card_body.appendChild(release_date);

        // RECUPERE L'ID AU CLIC
        poster_link.addEventListener('click', () => {
          sessionStorage.setItem('movie_id', id);
          sessionStorage.setItem('poster_path', poster_path);
          sessionStorage.setItem('title', title);
          sessionStorage.setItem('date', date);
          sessionStorage.setItem('score', average);
        });
      }
    });
};

// CHANGE GENRE
movies_btn.addEventListener('click', () => {
  genre = 'movie';
  localStorage.setItem('genre', genre);
  for (const deck of decks) {
    deck.innerText = '';
  }
  trend();
  top_rated();
});

series_btn.addEventListener('click', () => {
  genre = 'tv';
  localStorage.setItem('genre', genre);
  for (const deck of decks) {
    deck.innerText = '';
  }
  trend();
  top_rated();
});

// TENDANCES ONLOAD
window.onload = trend();

// TOP RATED ONLOAD
window.onload = top_rated();
