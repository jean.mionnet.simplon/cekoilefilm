# Cékoilefilm

Site web de films/séries, basé sur l'API de [TMDb](https://www.themoviedb.org/?language=fr)

## Prérequis

    * Ajouter votre clé d'accès TMDb aux fichiers JS dans la variable ** key **
    * Dans la console : ** npm install **

## Technologies utilisées

    * HTML5
    * CSS3
    * Animate.css
    * Bootstrap
    * JS
    * ProgressBar.js

## Outils utilisés

    * Node.js
    * eslint
    * stylelint

*La maquette de ce site web a été réalisée sur [figma](https://www.figma.com/file/GDGk7KmajQEma8Wd0wgYrx/main_frames?node-id=0%3A1)*
